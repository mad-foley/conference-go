import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_image(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}&page=1&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    picture = json.loads(response.content)

    image = {
        "picture_url": picture["photos"][0]["src"]['original']
    }

    return image


def get_current_weather(city, state):
    coords = get_coords(city, state)
    response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={coords['lat']}&lon={coords['lon']}&units=imperial&appid={OPEN_WEATHER_API_KEY}")
    content = json.loads(response.content)
    # return weather if available, else return null
    if content['main']['temp'] and content['weather'][0]["description"]:
        return {
            "temp": content['main']['temp'],
            "description": content['weather'][0]["description"]
        }
    else:
        return 'null'

def get_coords(city, state):
    response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid={OPEN_WEATHER_API_KEY}")
    content = json.loads(response.content)
    coords = {
        "lon": content[0]["lon"],
        "lat": content[0]["lat"]
    }
    return coords
